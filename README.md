# Computação Evolucionária

Curso do programa de mestrado em Modelagem Matemática e Computacional.

Código: MMC.004

Carga Horária: Total: 60 horas-aula semanal

Disciplina do módulo de formação específica (FE) e disciplina optativa (OP).


## Ementa
Conceitos básicos, evolução e seleção natural. Algoritmos Genéticos: conceituação, fundamentos matemáticos, aspectos computacionais, ambientes e técnicas de programação, paralelização de AG, aplicações. Introdução à Programação Genética. Introdução à Programação Evolucionária. Introdução à Estratégia Evolutiva. Computação Imunológica: elementos básicos do sistema imunológico, sistemas imunológicos artificiais, representação de antígenos e anticorpos, algoritmos imunológicos. Sistemas híbridos. Aplicações.

## Objetivos
- Apresentar os fundamentos teóricos da computação evolucionária.
- Conhecer e saber utilizar as principais técnicas e algoritmos da computação evolucionária. Exemplificar sua utilização no desenvolvimento de sistemas híbridos de otimização.
- Aplicar os algoritmos evolucionários em problemas combinatórios.

## Bibliografia Básica
- GASPAR-CUNHA, António, TAKAHASHI, Ricardo H. C., ANTUNES, Carlos Henggeler. Manual de Computação Evolutiva e Metaheurística, Imprensa da Universidade de Coimbra/Editora da Universidade Federal de Minas Gerais, 2013.
- A.E. EIBEN, J.E. SMITH, Introduction to Evolutionary Computing (Natural Computing Series), Springer 2008. ISBN 3540401849.
- BACK, Thomas. Evolutionary algorithms in theory and practice: evolution strategies evolutionary programming genetic algorithms. New York: Oxford University Press, 1996. 314 p. ISBN 0195099710.

## Bibliografia complementar
- KENNEDY, James F.; EBERHART, Russell C.; SHI, Yuhui. Swarm intelligence. San Francisco: Morgan Kaufmann Publishers, c2001. xxvii, 512 p. (The Morgan Kaufmann series in evolutionary computation) ISBN 1558605959.
- FOGEL, David B. Evolutionary computation: toward a new philosophy of machine intelligence. New York: IEEE Press, c1995.
- GOLDBERG, David E. (David Edward). Genetic algorithms in search optimization and machine learning. Reading, Mass.: Addison-Wesley, c1989. xiii,412p. ISBN 0201157675.
- COLEY, David A. An introduction to genetic algorithms for scientists and engineers. Singapore; River Edge, NJ: World Scientific, c1999. xvi, 227 p. ISBN 9789810236021.
- MICHALEWICZ, Zbigniew. Genetic algorithms + data systems = evolution programs. 3rd, rev. and extended ed. Berlin: Springer, c1996. 387 p. ISBN 3540606769.
- COELLO COELLO, Carlos A.; LAMONT, Gary B.; VAN VELDHUIZEN, David A.
Evolutionary algorithms for solving multi-objective problems. 2. ed. New York: Springer, 2007. 800 p. (Genetic and evolutionary computation series) ISBN 9780387332543.
- Artigos científicos da área.

---
Departamento de Computação - Av. Amazonas, 7675 - Belo Horizonte - MG - Brasil | Tel: 55 31 3319-6870

---

<p xmlns:dct="http://purl.org/dc/terms/" xmlns:vcard="http://www.w3.org/2001/vcard-rdf/3.0#">
  <a rel="license"
     href="http://creativecommons.org/publicdomain/zero/1.0/">
    <!-- <img src="http://i.creativecommons.org/p/zero/1.0/88x31.png" style="border-style: none;" alt="CC0" /> -->
    <img src="cc-zero.svg" style="border-style: none;" alt="CC0" />
  </a>
  <br />
  To the extent possible under law,
  <a rel="dct:publisher"
     href="https://framagit.org/rochamatcomp">
    <span property="dct:title">André Rocha</span></a>
  has waived all copyright and related or neighboring rights to
  <span property="dct:title">Mestrado de Modelagem Matemática e Computacional no CEFET-MG</span>.
This work is published from:
<span property="vcard:Country" datatype="dct:ISO3166"
      content="BR" about="https://framagit.org/rochamatcomp">
  Brazil</span>.
</p>